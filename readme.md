# Descrição

Pequeno repositório com o conteúdo relacionado com a master class do Factory Academy de Braga - Front-end À Séria

# Instruções

  * Verificar se têm instalado o **Node js** executando o comando na linha de comandos / terminal: `$ node -v`  
    * Se não estiver instalado, seguir instruções em: http://nodejs.org

  * Verificar se têm instalado o **Gulp** globalmente no seu computador executando o comando na linha de comandos / terminal: `$ gulp -v`  
    * Se não estiver instalado, correr o seguinte comando na linha de comandos / terminal: `$ npm install gulp -g`
    (certifique-se que tem permissões de administrador para instalar o **Gulp**)  

  * Descarregar como **zip** ou clonar o repositório localmente, navegar até à pasta criada através da linha de comandos / terminal e executar o seguinte comando:
    `$ npm install`

  * Executar na linha de comandos / terminal o seguinte comando:
    `gulp serve`

  * Pode agora alterar o código `JavaScript`, `HTML` e `CSS` que está na pasta `app` e verificar que o navegador actualiza automaticamente.

# Contacto

Qualquer dúvida, não hesites em contactar-me ;)  
`psoares.bj@gmail.com`
