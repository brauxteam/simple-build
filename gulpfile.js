const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const del = require('del');


// Clean dist folder
gulp.task('clean', function () {
    return del('dist');
});

browserSync.init({
    server: "./dist",
    open: false
});

//reloads the browser
function reload(done) {
    browserSync.reload();
    done();
}

function onError(err) {
    console.log(err);
    this.emit('end');
}

gulp.task('views', views);
function views() {
    return gulp.src(['./src/views/**/*.pug', '!**/_*.pug'])
        .pipe(pug({
            pretty: true
        }))
        .on('error', onError)
        .pipe(gulp.dest('dist'));
}

gulp.task('styles', styles);
function styles() {
    return gulp.src(['./src/styles/**/*.scss', '!**/_*.scss'])
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css'))
}

// Copy scripts folder to dist
gulp.task('scripts', scripts);
function scripts() {
    return gulp.src('src/scripts/**/*.js')
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'))
};

// Copy assets folder to dist
gulp.task('assets', assets);
function assets() {
    return gulp.src('src/assets/**/*.*')
        .pipe(gulp.dest('dist/assets'))
};

gulp.task('watch', watch);
function watch()  {
    gulp.watch('src/**/*.scss', gulp.series('styles', reload));
    gulp.watch('src/**/*.pug', gulp.series('views', reload));
    gulp.watch('src/**/*.js', gulp.series('scripts', reload));
    gulp.watch(['src/assets/**/*.*'], gulp.series('assets', reload));
}

gulp.task('prepare', gulp.series('styles', 'views', 'scripts', 'assets'));
gulp.task('default', gulp.series('clean', 'prepare', 'watch'));